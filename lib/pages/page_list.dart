import 'package:app_api_connect_test/components/common/component_custom_loading.dart';
import 'package:app_api_connect_test/components/common/component_no_contents.dart';
import 'package:app_api_connect_test/components/common/component_notification.dart';
import 'package:app_api_connect_test/model/goods_detail_item.dart';
import 'package:app_api_connect_test/repository/repo_goods.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:floating_bottom_navigation_bar/floating_bottom_navigation_bar.dart';
import 'package:flutter/material.dart';

class PageList extends StatefulWidget {
  const PageList({Key? key}) : super(key: key);

  @override
  State<PageList> createState() => _PageListState();
}

class _PageListState extends State<PageList> {
  List<GoodsDetailItem> _list = [];
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _getList('FOOD');
  }

  Future<void> _getList(String goodsCategory) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoGoods().getList('FOOD').then((res) {
      setState(() {
        _list = res.list;
        _totalItemCount = res.totalItemCount;
      });

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패햐였습니다.',
      ).call();

      BotToast.closeAllLoading();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('쇼핑'),
        centerTitle: true,
      ),
      bottomNavigationBar: FloatingNavbar(
        onTap: (int val) {
          //returns tab id which is user tapped
        },
        currentIndex: 0,
        items: [
          FloatingNavbarItem(icon: Icons.home, title: 'Home'),
          FloatingNavbarItem(icon: Icons.explore, title: 'Explore'),
          FloatingNavbarItem(icon: Icons.chat_bubble_outline, title: 'Chats'),
          FloatingNavbarItem(icon: Icons.settings, title: 'Settings'),
        ],
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    if (_totalItemCount <= 0) {
      return SizedBox(
        height: MediaQuery.of(context).size.height,
        child: const ComponentNoContents(icon: Icons.add_circle, msg: '상품이 없습니다.'),
      );
    } else {
      return SingleChildScrollView(
        child: Row(
          children: [
            const SizedBox(
              height: 10,
            ),
            for(int i = 0; i < _totalItemCount; i++)
              Row(
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: 250,
                    child: Image.asset('${_list[i].goodsImageUrl}'),
                  ),
                ],
              ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text('식품'),
                SizedBox(
                  width: 80,
                  height: 80,
                  child: Image.asset('assets/noodle.jpeg'),
                )
              ]
            ),
          ],
        ),
      );
    }
  }
}
