class GoodsUpdateRequest {
  String goodsName;

  GoodsUpdateRequest(this.goodsName);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = Map<String, dynamic>();

    result['goodsName'] = this.goodsName;

    return result;
  }
}