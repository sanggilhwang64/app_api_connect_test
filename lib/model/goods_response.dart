import 'package:app_api_connect_test/model/goods_detail_item.dart';

class GoodsResponse {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<GoodsDetailItem> list;

  GoodsResponse(
      this.isSuccess,
      this.code,
      this.msg,
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      this.list
      );

  factory GoodsResponse.fromJson(Map<String, dynamic> json) {
    return GoodsResponse(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] == null ? [] : (json['list'] as List).map((e) => GoodsDetailItem.fromJson(e)).toList()
    );
  }
}