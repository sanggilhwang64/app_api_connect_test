class GoodsCreateRequest {
  String goodsName;
  double costPrice;
  double salePrice;

  GoodsCreateRequest(this.goodsName, this.costPrice, this.salePrice);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = Map<String, dynamic>();

    result['goodsName'] = this.goodsName;
    result['costPrice'] = this.costPrice;
    result['salePrice'] = this.salePrice;

    return result;
  }
}