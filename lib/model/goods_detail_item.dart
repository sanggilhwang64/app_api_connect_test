class GoodsDetailItem {
  int id;
  String goodsCategory;
  String goodsImageUrl;
  String goodsName;
  double costPrice;
  double salePrice;
  double totalPrice;

  GoodsDetailItem(
      this.id,
      this.goodsCategory,
      this.goodsImageUrl,
      this.goodsName,
      this.costPrice,
      this.salePrice,
      this.totalPrice
      );

  factory GoodsDetailItem.fromJson(Map<String, dynamic> json) {
    return GoodsDetailItem(
      json['id'],
      json['goodsCategory'],
      json['goodsImageUrl'],
      json['goodsName'],
      json['costPrice'],
      json['salePrice'],
      json['totalPrice'],
    );
  }
}