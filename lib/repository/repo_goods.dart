import 'package:app_api_connect_test/model/common_response.dart';
import 'package:app_api_connect_test/model/goods_create_request.dart';
import 'package:app_api_connect_test/model/goods_response.dart';
import 'package:app_api_connect_test/model/goods_update_request.dart';
import 'package:dio/dio.dart';

class RepoGoods {
  Future<CommonResponse> setData(GoodsCreateRequest request) async {
    const String baseUrl = 'http://localhost:8080/v1/goods/data';

    Dio dio = Dio();

    final response = await dio.post(
      baseUrl,
      data: request.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return CommonResponse.fromJson(response.data);
  }

  Future<GoodsResponse> getList(String goodsCategory) async {
    const String baseUrl = 'http://localhost:8080/v1/goods/all/category';

    Map<String, dynamic> params = {};
    params['goodsCategory'] = goodsCategory;

    Dio dio = Dio();

    final response = await dio.get(
      baseUrl,
      queryParameters: params,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return GoodsResponse.fromJson(response.data);
  }

  Future<CommonResponse> putData(int goodsId, GoodsUpdateRequest request) async {
    const String baseUrl = 'http://localhost:8080/v1/goods/name/goods-id/{goodsId}';

    Dio dio = Dio();

    final response = await dio.put(
      baseUrl.replaceAll('{goodsId}', goodsId.toString()),
      data: request.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return CommonResponse.fromJson(response.data);
  }

  Future<CommonResponse> delData(int goodsId) async {
    const String baseUrl = 'http://localhost:8080/v1/goods/goods-id/{goodsId}';

    Dio dio = Dio();

    final response = await dio.delete(
      baseUrl.replaceAll('{goodsId}', goodsId.toString()),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      )
    );

    return CommonResponse.fromJson(response.data);
  }
}